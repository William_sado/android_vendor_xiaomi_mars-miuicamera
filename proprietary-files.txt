# All unpinned blobs below are extracted from miui_STAR_V14.0.11.0.TKACNXM

-product/priv-app/MiuiCamera/MiuiCamera.apk:system/priv-app/MiuiCamera/MiuiCamera.apk;OVERRIDES=GrapheneCamera,Camera,Camera2,Flash,Aperture
system/lib64/android.hardware.camera.common@1.0.so
system/lib64/libcamera_algoup_jni.xiaomi.so
system/lib64/libcamera_mianode_jni.xiaomi.so
vendor/lib64/libcdsprpc.so:system/lib64/libcdsprpc.so
system/lib64/libdoc_photo.so|aafd99b1414ac5e2439b503f9e3c16e041f1e12f
system/lib64/libdoc_photo_c++_shared.so|8bd494c3b1da5d250e0dc81231ea094fd0bccf88
system/lib64/libhidltransport.so
vendor/lib64/libmialgo_ai_vision.so:system/lib64/libmialgo_ai_vision.so
system/lib64/libmicampostproc_client.so
system/lib64/libmotion_photo.so|1d58550ec98df90d585af82ede89db4d4504decd
system/lib64/libmotion_photo_c++_shared.so|8bd494c3b1da5d250e0dc81231ea094fd0bccf88
system/lib64/libmotion_photo_mace.so|ab99cb01df8996ae250f718113bc97031f603a6f
system_ext/lib64/libopencl-camera.so:system/lib64/libopencl-camera.so
vendor/lib64/vendor.qti.hardware.dsp@1.0.so:system/lib64/vendor.qti.hardware.dsp@1.0.so
system/lib64/vendor.xiaomi.hardware.campostproc@1.0.so
system/lib64/libc++_shared.so|ef6cad49e61581e9fa96cae94a8e101077640f78
vendor/lib64/libSNPE.so:system/lib64/libSNPE.so
system/lib64/libion.so
system/lib64/android.hardware.graphics.allocator@3.0.so:vendor/lib64/android.hardware.graphics.allocator@3.0.so

# miuilib
vendor/lib64/libOpenCL.so:system/priv-app/MiuiCamera/lib/arm64/libOpenCL.so

# libcameraserver
system/system/lib64/android.hardware.camera.common@1.0.so
system/system/lib64/libcameraservice.so
vendor/lib64/vendor.qti.hardware.camera.device@1.0.so
vendor/lib64/vendor.qti.hardware.camera.device@3.5.so
vendor/lib/vendor.qti.hardware.camera.device@1.0.so
vendor/lib/vendor.qti.hardware.camera.device@3.5.so

#Camera Device lib
vendor/lib64/camera.device@1.0-impl.so
vendor/lib64/camera.device@3.2-impl.so
vendor/lib64/camera.device@3.3-impl.so
vendor/lib64/camera.device@3.4-external-impl.so
vendor/lib64/camera.device@3.4-impl.so
vendor/lib64/camera.device@3.5-external-impl.so
vendor/lib64/camera.device@3.5-impl.so
vendor/lib64/camera.device@3.6-external-impl.so

# Camera provider
vendor/lib/hw/android.hardware.camera.provider@2.4-impl.so
vendor/lib64/hw/android.hardware.camera.provider@2.4-impl.so
vendor/lib/android.hardware.camera.provider@2.4-legacy.so
vendor/lib64/android.hardware.camera.provider@2.4-legacy.so


